import Config

config :storehouse, ecto_repos: [Storehouse.Repo]

import_config "#{Mix.env()}.exs"
