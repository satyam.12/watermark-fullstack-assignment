import Config

config :storehouse, Storehouse.Repo,
  database: "todos_dev",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"
