defmodule Storehouse do
  @moduledoc """
  Documentation for `Storehouse`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Storehouse.hello()
      :world

  """
  def hello do
    :world
  end
end
