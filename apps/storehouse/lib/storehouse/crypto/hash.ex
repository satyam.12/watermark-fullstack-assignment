defmodule Storehouse.Crypto.Hash do
  @doc false
  def generate(string) do
    :sha256
    |> :crypto.hash(string)
    |> Base.encode16()
    |> String.downcase()
  end
end
