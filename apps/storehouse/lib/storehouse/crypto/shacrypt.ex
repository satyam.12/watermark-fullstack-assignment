defmodule Storehouse.Crypto.Shacrypt do
  @random_salt_bytes 16

  def hash_pwd_salt(string) do
    salt = random_salt()

    salt <> Storehouse.Crypto.Hash.generate(salt <> string)
  end

  def verify_pass(password, <<salt::binary-size(@random_salt_bytes), hash::binary>>) when is_binary(password) do
    Storehouse.Crypto.Hash.generate(salt <> password) == hash
  end
  def verify_pass(_, _), do: false

  def no_user_verify do
    hash_pwd_salt("")

    false
  end

  defp random_salt do
    :crypto.strong_rand_bytes(@random_salt_bytes)
    |> Base.encode64
    |> binary_part(0, @random_salt_bytes)
  end
end
