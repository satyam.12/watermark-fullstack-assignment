defmodule Storehouse.Entity.Todos do
  @moduledoc """
  The Todos context.
  """

  import Ecto.Query, warn: false

  alias Storehouse.{
    Repo,
    Model.Todos
  }

  def list_todos do
    Repo.all(Todos)
  end

  def get_by(attrs) do
    Repo.get_by(Todos, attrs)
  end

  def get_todo!(id), do: Repo.get!(Todos, id)

  def create_todo(attrs \\ %{}) do
    %Todos{}
    |> Todos.changeset(attrs)
    |> Repo.insert()
  end

  def update_todo(%Todos{} = todo, attrs) do
    todo
    |> Todos.changeset(attrs)
    |> Repo.update()
  end

  def delete_todo(%Todos{} = todo) do
    Repo.delete(todo)
  end

  def change_todo(%Todos{} = todo, attrs \\ %{}) do
    Todos.changeset(todo, attrs)
  end
end
