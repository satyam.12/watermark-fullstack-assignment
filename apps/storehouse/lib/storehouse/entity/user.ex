defmodule Storehouse.Entity.User do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false

  alias Storehouse.{
    Repo,
    Model.User
  }

  def list_users do
    Repo.all(User)
  end

  def get_by(attrs) do
    Repo.get_by(User, attrs)
  end

  def get_user!(id), do: Repo.get!(User, id)

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc false
  def update_user_password(%{"username" => username} = attrs) do
    case get_by(username: username) do
      nil ->
        :error

      user ->
        user
        |> User.changeset(attrs)
        |> Repo.update()
    end
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end
end
