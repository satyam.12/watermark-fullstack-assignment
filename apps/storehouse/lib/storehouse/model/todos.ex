defmodule Storehouse.Model.Todos do
  use Ecto.Schema
  import Ecto.Changeset

  @fields ~w"text completed user_id"a

  schema "todos" do
    field :text, :string
    field :completed, :boolean, default: false

    belongs_to :user, Storehouse.Model.User

    timestamps()
  end

  @doc false
  def changeset(user_device, attrs) do
    user_device
    |> cast(attrs, @fields)
    |> validate_required([:text, :user_id])
  end
end
