defmodule Storehouse.Model.User do
  use Ecto.Schema

  import Ecto.Changeset

  alias Storehouse.Crypto

  @fields ~w"password username"a

  schema "users" do
    field :password_hash, :string, redact: true
    field :username, :string
    field :password, :string, virtual: true, redact: true

    has_many :todo_list, Storehouse.Model.Todos

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> update_password_changeset(attrs)
  end

  @doc false
  def update_password_changeset(user, attrs) do
    user
    |> cast(attrs, @fields)
    |> validate_required(@fields)
    |> validate_password()
    |> hash_password()
  end

  @doc false
  defp validate_password(changeset) do
    changeset
    |> validate_length(:password, min: 8, max: 14)
    |> validate_format(:password, ~r/[0-9]+/, message: "Password must contain a number") # has a number
    |> validate_format(:password, ~r/[A-Z]+/, message: "Password must contain an upper-case letter") # has an upper case letter
    |> validate_format(:password, ~r/[a-z]+/, message: "Password must contain a lower-case letter") # has a lower case letter
    |> validate_format(:password, ~r/[#\!\?&@\$%^&*\(\)]+/, message: "Password must contain a symbol") #has a special symbol
  end

  @doc false
  defp hash_password(changeset) do
    password = get_change(changeset, :password)

    if changeset.valid? do
      changeset
      |> put_change(:password_hash, Crypto.Shacrypt.hash_pwd_salt(password))
      |> delete_change(:password)
    else
      changeset
    end
  end
end
