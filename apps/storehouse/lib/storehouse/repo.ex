defmodule Storehouse.Repo do
  use Ecto.Repo,
    otp_app: :storehouse,
    adapter: Ecto.Adapters.Postgres
end
