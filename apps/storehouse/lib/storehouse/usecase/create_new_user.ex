defmodule Storehouse.Usecase.CreateNewUser do
  alias Storehouse.Entity.User

  def perform(%{"username" => username, "password" => _} = attrs) do
    with nil <- User.get_by(username: username),
         {:ok, _user} <- User.create_user(attrs)
    do
      :ok

    else
      {:error, _msg} -> {:error, "Invalid username / password"}

      _result -> {:error, "User already exists"}
    end
  end
  def perform(_), do: {:error, "Invalid username / password"}
end
