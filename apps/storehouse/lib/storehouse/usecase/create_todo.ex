defmodule Storehouse.Usecase.CreateTodo do
  alias Storehouse.{
    Entity.User,
    Entity.Todos
  }

  def perform(%{"username" => username} = attrs) do
    with user when not is_nil(user) <- User.get_by(username: username),
        attrs <- Map.put(attrs, "user_id", user.id),
        {:ok, todo} <- Todos.create_todo(attrs),
        formatted_todo <- transform_to_map(todo)
    do
      {:ok, formatted_todo}

    else
      {:error, msg} -> {:error, msg}

      _ ->
        {:error, "User does not exists"}
    end
  end
  def perform(_), do: {:error, "User does not exists"}

  defp transform_to_map(todo) do
    Map.from_struct(todo)
    |> Map.drop([:__meta__, :inserted_at, :updated_at, :user, :user_id])
  end
end
