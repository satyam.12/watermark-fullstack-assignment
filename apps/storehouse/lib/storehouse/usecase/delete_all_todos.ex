defmodule Storehouse.Usecase.DeleteAllTodos do
  alias Storehouse.{
    Entity.User,
    Entity.Todos,
    Repo
  }

  def perform(%{"username" => username}) do
    with user when not is_nil(user) <- User.get_by(username: username) |> Repo.preload([:todo_list]),
        todos <- user.todo_list
    do
      todos
      |> Enum.map(fn todo ->
        Todos.delete_todo(todo)
      end)

      {:ok, "Items deleted"}

    else
      _ ->
        {:error, "Items does not exists"}
    end
  end
  def perform(_), do: {:error, "Items does not exists"}
end
