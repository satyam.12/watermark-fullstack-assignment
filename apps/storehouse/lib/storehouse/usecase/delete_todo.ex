defmodule Storehouse.Usecase.DeleteTodo do
  alias Storehouse.{
    Entity.User,
    Entity.Todos
  }

  def perform(%{"username" => username, "id" => id}) do
    with user when not is_nil(user) <- User.get_by(username: username),
        todo when not is_nil(todo) <- Todos.get_by(id: id, user_id: user.id),
        {:ok, _} <- Todos.delete_todo(todo)
    do
      {:ok, "Item deleted"}

    else
      _ ->
        {:error, "Item does not exists"}
    end
  end
  def perform(_), do: {:error, "Item does not exists"}
end
