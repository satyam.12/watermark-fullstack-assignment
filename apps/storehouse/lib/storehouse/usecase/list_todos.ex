defmodule Storehouse.Usecase.ListTodos do
  alias Storehouse.{
    Entity.User,
    Repo
  }

  def get(%{"username" => username}) do
    with user when not is_nil(user) <- User.get_by(username: username) |> Repo.preload([:todo_list]),
        todo_list when is_list(todo_list) and todo_list != [] <- user.todo_list,
        todos <- transform_to_map(todo_list)
    do
      {:ok, todos}

    else
      _ ->
        {:error, "Invalid username"}
    end
  end
  def get(_), do: {:error, "Invalid username"}

  defp transform_to_map(todo_list) do
    todo_list
    |> Enum.map(fn todo ->
      Map.from_struct(todo)
      |> Map.drop([:__meta__, :inserted_at, :updated_at, :user, :user_id])
    end)
  end
end
