defmodule Storehouse.Usecase.UpdateTodo do
  alias Storehouse.{
    Entity.User,
    Entity.Todos
  }

  def perform(%{"username" => username, "id" => id} = attrs) do
    with user when not is_nil(user) <- User.get_by(username: username),
        todo when not is_nil(todo) <- Todos.get_by(id: id, user_id: user.id),
        {:ok, updated_todo} <- Todos.update_todo(todo, attrs),
        formatted_todo <- transform_to_map(updated_todo)
    do
      {:ok, formatted_todo}

    else
      _ ->
        {:error, "Item does not exists"}
    end
  end
  def perform(_), do: {:error, "Item does not exists"}

  defp transform_to_map(todo) do
    Map.from_struct(todo)
    |> Map.drop([:__meta__, :inserted_at, :updated_at, :user, :user_id])
  end
end
