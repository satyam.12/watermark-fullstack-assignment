defmodule Storehouse.Usecase.VerifyUserPassword do
  alias Storehouse.{
    Entity.User,
    Crypto
  }

  def verify(%{"username" => username, "password" => password}) do
    with user when not is_nil(user) <- User.get_by(username: username),
         true <- Crypto.Shacrypt.verify_pass(password, user.password_hash)
    do
      :ok

    else
      {:error, _msg} -> {:error, "Invalid username / password"}

      result ->
        unless result, do: Crypto.Shacrypt.no_user_verify()

        {:error, "Invalid username / password"}
    end
  end
  def verify(_), do: {:error, "Invalid username / password"}
end
