defmodule Storehouse.Repo.Migrations.TodosTable do
  use Ecto.Migration

  def change do
    create table(:todos) do
      add :text, :string
      add :completed, :boolean, default: false

      add :user_id, references(:users)

      timestamps()
    end

    create unique_index(:todos, [:id])
  end
end
