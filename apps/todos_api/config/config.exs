# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :todos_api, TodosApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "LijnD+hE314eHoo+US818sSgKX4CtC+oVykvd2Vsc297dJUDy3TX3ugSmdODxvtD",
  render_errors: [view: TodosApiWeb.ErrorView, accepts: ~w(json), layout: false],
  server: true,
  pubsub_server: TodosApi.PubSub,
  live_view: [signing_salt: "crqThwaO"]

# Joken configuration
config :todos_api, :session_token,
  secret: "this is secret"

config :cors_plug,
  origin: [
    "http://localhost:4000",
    "http://localhost:4001",
    "http://127.0.0.1:4000",
    "http://127.0.0.1:4001"
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
