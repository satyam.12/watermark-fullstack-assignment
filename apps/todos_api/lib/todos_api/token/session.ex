defmodule TodosApi.Token.SessionToken do
  @moduledoc false
  use Joken.Config

  @signer_alg "HS512"
  @expiry 60 * 60 * 24

  @doc false
  def token_config do
    default_claims(default_exp: @expiry, skip: [:iat, :nbf, :iss, :aud, :jti])
  end

  @doc false
  def sign do
    Joken.Signer.create(@signer_alg, secret())
  end

  @doc false
  def secret do
    Application.fetch_env!(:todos_api, :session_token)[:secret]
  end

  @doc false
  def validate_and_verify(token) do
    Joken.verify_and_validate(token_config(), token, sign())
  end

  @doc false
  def sign_and_generate(data) when is_map(data) do
    Joken.generate_and_sign(token_config(), data, sign())
  end
end
