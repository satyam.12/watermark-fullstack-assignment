defmodule TodosApiWeb.LoginController do
  use TodosApiWeb, :controller

  alias TodosApi.Token.SessionToken
  alias Storehouse.Usecase.{
    VerifyUserPassword,
    CreateNewUser
  }

  @doc false
  def verify_user_password(conn, %{"username" => _, "password" => _} = params) do
    case VerifyUserPassword.verify(params) do
      :ok ->
        {:ok, session_token, _} =
          params
          |> build_payload()
          |> SessionToken.sign_and_generate()

        response = build_response(params, session_token)

        conn
        |> render("response.json", %{response: response})

      {:error, msg} ->
        conn
        |> put_status(:unauthorized)
        |> render("response.json", %{response: %{msg: msg}})
    end
  end
  def verify_user_password(conn, _) do
    conn
    |> put_status(:unauthorized)
    |> render("response.json", %{response: %{msg: "Invalid username / password"}})
  end

  @doc false
  def create_new_user(conn, %{"username" => _, "password" => _} = params) do
    case CreateNewUser.perform(params) do
      :ok ->
        {:ok, session_token, _} =
          params
          |> build_payload()
          |> SessionToken.sign_and_generate()

        response = build_response(params, session_token)

        conn
        |> render("response.json", %{response: response})

      {:error, msg} ->
        conn
        |> put_status(:unauthorized)
        |> render("response.json", %{response: %{msg: msg}})
    end
  end
  def create_new_user(conn, _) do
    conn
    |> put_status(:unauthorized)
    |> render("response.json", %{response: %{msg: "Invalid username / password"}})
  end

  defp build_payload(%{"username" => username}), do: %{"username" => username}

  defp build_response(%{"username" => username}, token) do
    %{
      session_token: token,
      username: username
    }
  end
end
