defmodule TodosApiWeb.TodosController do
  use TodosApiWeb, :controller

  alias Storehouse.Usecase.{
    ListTodos,
    UpdateTodo,
    DeleteTodo,
    DeleteAllTodos,
    CreateTodo
  }

  @doc false
  def list_todos(conn, %{"username" => _} = params) do
    case ListTodos.get(params) do
      {:ok, todos} ->
        conn
        |> render("response.json", %{response: %{ todos: todos}})

      {:error, msg} ->
        conn
        |> put_status(:unauthorized)
        |> render("response.json", %{response: %{msg: msg}})
    end
  end
  def list_todos(conn, _) do
    conn
    |> put_status(:unauthorized)
    |> render("response.json", %{response: %{msg: "Unauthorized"}})
  end

  def create_todo(conn, %{"username" => _, "text" => _, "completed" => _} = params) do
    case CreateTodo.perform(params) do
      {:ok, todo} ->
        conn
        |> render("response.json", %{response: todo})

      {:error, msg} ->
        conn
        |> put_status(:unauthorized)
        |> render("response.json", %{response: %{msg: msg}})
    end
  end
  def create_todo(conn, _) do
    conn
    |> put_status(:unauthorized)
    |> render("response.json", %{response: %{msg: "Unauthorized"}})
  end

  def update_todo(conn, %{"username" => _, "id" => _, "text" => _, "completed" => _} = params) do
    case UpdateTodo.perform(params) do
      {:ok, todo} ->
        conn
        |> render("response.json", %{response: todo})

      {:error, msg} ->
        conn
        |> put_status(:unauthorized)
        |> render("response.json", %{response: %{msg: msg}})
    end
  end
  def update_todo(conn, _) do
    conn
    |> put_status(:unauthorized)
    |> render("response.json", %{response: %{msg: "Unauthorized"}})
  end

  def delete_todo(conn, %{"username" => _, "id" => _} = params) do
    case DeleteTodo.perform(params) do
      {:ok, msg} ->
        conn
        |> render("response.json", %{response: %{ msg: msg}})

      {:error, msg} ->
        conn
        |> put_status(:unauthorized)
        |> render("response.json", %{response: %{msg: msg}})
    end
  end
  def delete_todo(conn, _) do
    conn
    |> put_status(:unauthorized)
    |> render("response.json", %{response: %{msg: "Unauthorized"}})
  end

  def delete_all_todos(conn, %{"username" => _} = params) do
    case DeleteAllTodos.perform(params) do
      {:ok, msg} ->
        conn
        |> render("response.json", %{response: %{ msg: msg}})

      {:error, msg} ->
        conn
        |> put_status(:unauthorized)
        |> render("response.json", %{response: %{msg: msg}})
    end
  end
  def delete_all_todos(conn, _) do
    conn
    |> put_status(:unauthorized)
    |> render("response.json", %{response: %{msg: "Unauthorized"}})
  end
end
