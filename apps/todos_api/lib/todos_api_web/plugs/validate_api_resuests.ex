defmodule TodosApiWeb.Plugs.ValidateApiResuests do
  @moduledoc false

  import Plug.Conn

  alias TodosApi.Token
  @spec init(any) :: any
  def init(opts), do: opts

  @spec call(Plug.Conn.t(), any) :: Plug.Conn.t()
  def call(conn, _opts) do
    case validate_request(conn) do
      {:ok, conn} ->
        conn

      _ ->
        conn
        |> send_resp(:unauthorized, "Unauthorized")
        |> halt
    end
  end

  @spec validate_request(Plug.Conn.t()) :: {:ok, Plug.Conn.t()} | :error
  def validate_request(%Plug.Conn{params: %{"username" => username, "session_token" => session_token}} = conn) do
    case Token.SessionToken.validate_and_verify(session_token) do
      {:ok, %{"username" => ^username}} ->
        {:ok, conn}

      _ ->
        :error
    end
  end
  def validate_request(_), do: :error
end
