defmodule TodosApiWeb.Router do
  use TodosApiWeb, :router

  pipeline :public_api do
    plug :accepts, ["json"]
  end

  pipeline :protected_api do
    plug TodosApiWeb.Plugs.ValidateApiResuests
  end

  scope "/api", TodosApiWeb do
    pipe_through :public_api

    post "/login", LoginController, :verify_user_password
    post "/signup", LoginController, :create_new_user

    scope "/todos" do
      pipe_through :protected_api

      post "/all", TodosController, :list_todos
      post "/all/delete", TodosController, :delete_all_todos
      post "/new", TodosController, :create_todo
      post "/:id/update", TodosController, :update_todo
      post "/:id/delete", TodosController, :delete_todo
    end
  end
end
