defmodule TodosApiWeb.TodosView do
  use TodosApiWeb, :view

  def render("response.json", %{response: data}) do
    data
  end
end
