module Helper exposing (..)

import Http
import Json.Encode
import Models exposing (..)
import Msgs exposing (..)
import Models exposing (..)

-- Helper Functions

userLogin : Model -> Cmd Msg
userLogin model =
    let
        params = { url = baseLoginUrl
                 , body = Http.jsonBody <|
                            Json.Encode.object
                                [ ( "username", Json.Encode.string model.username )
                                , ( "password", Json.Encode.string model.password )
                                ]
                , expect = Http.expectJson UserResponse userDecoder
                }
    in
        Http.post params

userSignup : Model -> Cmd Msg
userSignup model =
    let
        params = { url = baseSignupUrl
                 , body = Http.jsonBody <|
                            Json.Encode.object
                                [ ( "username", Json.Encode.string model.username )
                                , ( "password", Json.Encode.string model.password )
                                ]
                , expect = Http.expectJson UserResponse userDecoder
                }
    in
        Http.post params

fetchTodos : Model -> Cmd Msg
fetchTodos model =
    let
        params = { url = baseTodoUrl ++ "all"
                 , body = Http.jsonBody <| 
                    Json.Encode.object
                        [( "username", Json.Encode.string model.username )
                        , ( "session_token", Json.Encode.string model.session_token )
                        ]
                 , expect = Http.expectJson Msgs.TodosResponse Models.responseDecoder
                }
    in
        Http.post params  


deleteTodos : Model -> Cmd Msg
deleteTodos model =
    let
        params = { url = baseTodoUrl ++ "all/delete"
                 , body = Http.jsonBody <| 
                    Json.Encode.object
                        [( "username", Json.Encode.string model.username )
                        , ( "session_token", Json.Encode.string model.session_token )
                        ]
                 , expect = Http.expectJson StatusMsgResponse statusDecoder
                }
    in
        Http.post params    

addTodo : Model -> Cmd Msg
addTodo model =
    let
        params = { url = baseTodoUrl ++ "new"
                , body = Http.jsonBody <|
                            Json.Encode.object
                                [ ( "text", Json.Encode.string model.input )
                                , ( "completed", Json.Encode.bool False )
                                , ( "username", Json.Encode.string model.username )
                                , ( "session_token", Json.Encode.string model.session_token )
                                ]
                , expect = Http.expectJson NewTodoResponse todosDecoder
                }
    in
        Http.post params

removeTodo : Int -> Model -> Cmd Msg
removeTodo id model =
    let
        params = { url = baseTodoUrl ++ (id |> String.fromInt) ++ "/delete"
                , body = Http.jsonBody <|
                            Json.Encode.object
                                [ ( "username", Json.Encode.string model.username ) 
                                , ( "session_token", Json.Encode.string model.session_token )
                                ]
                , expect = Http.expectJson StatusMsgResponse statusDecoder
                }
    in
        Http.post params


updateTodo : Int -> Todos -> Model -> Cmd Msg
updateTodo id todo model =
    let
        params = { url = baseTodoUrl ++ (id |> String.fromInt) ++ "/update"
                , body = Http.jsonBody <|
                            Json.Encode.object
                                [ ( "username", Json.Encode.string model.username )
                                , ( "text", Json.Encode.string todo.text )
                                , ( "completed", Json.Encode.bool todo.completed )
                                , ( "session_token", Json.Encode.string model.session_token )
                                ]
                , expect = Http.expectJson StatusMsgResponse statusDecoder
                }
    in
        Http.post params
