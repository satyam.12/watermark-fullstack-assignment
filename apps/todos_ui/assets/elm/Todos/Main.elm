module Main exposing (..)

import Browser
import Msgs exposing (..)
import Models exposing (Model, init)
import View exposing (view)
import Update exposing (update)

-- Main

main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = (\_ -> Sub.none)
        }