module Models exposing (..)

import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (..)

-- Models

type alias Todos =
    { completed : Bool
    , id : Int
    , text : String
    , editing : Bool
    }

type alias TodosResponse =
    { todos : List Todos
    }

type alias User =
    { session_token : String
    , username : String
    }

type alias Status =
    { id : Int
    , msg : String
    }

type alias Model =
    { session_token : String
    , username : String
    , todos : List Todos
    , input : String
    , password : String
    , signup : Bool
    }

baseLoginUrl : String
baseLoginUrl = "http://0.0.0.0:8000/api/login"

baseSignupUrl : String
baseSignupUrl = "http://0.0.0.0:8000/api/signup"

baseTodoUrl : String
baseTodoUrl = "http://127.0.0.1:8000/api/todos/"

-- Decoders

statusDecoder : Decoder Status
statusDecoder =
    Json.Decode.succeed Status
    |> Json.Decode.Pipeline.required "id" int
    |> Json.Decode.Pipeline.required "msg" string

todosDecoder : Decoder Todos
todosDecoder =
    Json.Decode.succeed Todos
    |> Json.Decode.Pipeline.required "completed" bool
    |> Json.Decode.Pipeline.required "id" int
    |> Json.Decode.Pipeline.required "text" string
    |> Json.Decode.Pipeline.hardcoded False

responseDecoder : Decoder TodosResponse
responseDecoder =
    Json.Decode.succeed TodosResponse
    |> Json.Decode.Pipeline.required "todos" (list todosDecoder)

userDecoder : Decoder User
userDecoder = 
    Json.Decode.succeed User
    |> Json.Decode.Pipeline.required "session_token" string
    |> Json.Decode.Pipeline.required "username" string

init : () -> ( Model, Cmd msg)
init _ =
    ({ session_token = ""
     , username = ""
     , todos = []
     , password = ""
     , input = ""
     , signup = False
    }
    , Cmd.none)
