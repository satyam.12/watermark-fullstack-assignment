module Msgs exposing (..)

import Http
import Models exposing (..)

type Msg
    = Fetch
    | Login
    | SignUp
    | ToggleSignUp
    | Logout
    | Clear
    | AddTodo
    | ChangeInput String
    | EditTodo Int
    | RemoveTodo Int
    | ToggleTodoStatus Int
    | UpdateTodo Int
    | EditTodoInput Int String
    | ChangeUsername String
    | ChangePassword String
    | UserResponse (Result Http.Error User)
    | TodosResponse (Result Http.Error TodosResponse)
    | NewTodoResponse (Result Http.Error Todos)
    | StatusMsgResponse (Result Http.Error Status)