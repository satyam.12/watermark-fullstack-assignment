module Update exposing (..)


import Msgs exposing (..)
import Models exposing (..)
import Helper exposing (..)
import Update.Extra exposing (..)

-- Update

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Clear -> 
            ( { model | todos = [] }, deleteTodos model )

        ChangeUsername input ->
            ( { model | username = input }, Cmd.none )

        ChangePassword input ->
            ( { model | password = input }, Cmd.none )

        ChangeInput input ->
            ( { model | input = input }, Cmd.none )

        UserResponse response ->
            case response of
                Ok data ->
                    ( { model | username = data.username, signup = False, session_token = data.session_token }, Cmd.none )
                    |> andThen update Fetch

                Err _ ->
                    ( model, Cmd.none )

        Msgs.TodosResponse response ->
            case response of
                Ok data ->
                    ( { model | todos = data.todos }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        NewTodoResponse response ->
            case response of
                Ok data ->
                    ( { model | todos = model.todos ++ (List.singleton data) }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        StatusMsgResponse response ->
            case response of
                Ok _ ->
                    ( model, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        Fetch ->
            ( model, fetchTodos model )

        Msgs.Login ->
            ( { model | password = "" }, userLogin model )

        SignUp ->
            ( { model | password = "" }, userSignup model )

        ToggleSignUp ->
            ( { model | signup = not model.signup }, Cmd.none )
        
        Logout ->
            Models.init ()

        AddTodo ->
            ({ model | input = "" }, addTodo model )

        RemoveTodo id ->
            ({ model | todos = List.filter (\ item -> item.id /= id ) model.todos }, removeTodo id model)

        EditTodo id ->
            ({ model | todos = List.map (\ item -> 
                if item.id == id then
                    { item | editing = not item.editing, text = item.text }
                else
                    item ) model.todos }, Cmd.none )
        
        EditTodoInput id input ->
            ( { model | todos = List.map (\ item ->
                if item.id == id then
                    { item | text = input }
                else
                    item ) model.todos }, Cmd.none )

        UpdateTodo id ->
            let
                todo_item = List.head <| List.filter (\ item -> item.id == id) model.todos
            in
                case todo_item of
                    Just todo -> ( { model | todos = List.map (\ item -> 
                                    if item.id == id then
                                        { item | editing = not item.editing }
                                    else
                                        item ) model.todos }, updateTodo id todo model )
                    Nothing -> ( model, Cmd.none )
        
        ToggleTodoStatus id ->
            let
                todo_item = List.head <| List.filter (\ item -> item.id == id) model.todos
            in
                case todo_item of
                    Just todo -> (  { model | todos = List.map (\ item -> 
                                    if item.id == id then
                                        { item | completed = not item.completed }
                                    else
                                        item ) model.todos }, updateTodo id todo model )
                    Nothing -> ( model, Cmd.none )