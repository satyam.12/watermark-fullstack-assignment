module View exposing (..)

import Msgs exposing (..)
import Models exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

-- View

view : Model -> Html Msg
view model =
    swicthView model

swicthView : Model -> Html Msg
swicthView model =
     if model.session_token == "" && model.signup == False then
        loginFormView model
     else if model.session_token == "" && model.signup == True then
        signupFormView model
      else
        todosView model

todosView : Model -> Html Msg
todosView model =
    div
        [ class "container" ]
        [ greetUser model
        , br [] [] 
        , h1 
            [ style "color" "red"
            , style "text-align" "center"
            , style "margin-top" "10rem"
            , style "font-size" "10rem" ] 
            [ text "todos" ]
        , p 
            [ style "color" "grey"
            , style "text-align" "center"
            , style "font-size" "3rem" ] 
            [ text "A simple todo-app build using Elixir & Elm." ]
        , br [] []
        , Html.form 
            [ class "elm-form", onSubmit AddTodo
            , style "width" "100%" ]
            [ todoInput model
            , clearButton
            ]
        , viewTodos model
        ]

greetUser : Model -> Html Msg
greetUser model =
    div 
        [ style "width" "100%"
        , style "text-align" "left"
        , style "font-size" "2.5rem"
        ]
        [ text ("Welcome " ++ model.username)
        , logoutButton
        ]

todoInput : Model -> Html Msg
todoInput model =
    input
        [ type_ "text"
        , placeholder "What needs to be done?"
        , value model.input
        , onInput ChangeInput
        , style "color" "grey"
        , style "font-size" "3.5rem"
        , style "width" "100%"
        , style "height" "15%"
        , style "padding" "10px"
        , style "border-radius" "4px"
        , style "border" "1px solid #333"
        ]
        []

viewTodos : Model -> Html Msg
viewTodos model =
    if List.isEmpty model.todos then
        ul [] []
    else
        ul
            [ style "list-style" "none"
            , style "margin-bottom" "20px"
            , style "width" "100%"
            , style "width" "100%"
            , style "font-size" "2.5rem"
            ]
            (List.map viewTodo model.todos)

viewTodo : Todos -> Html Msg
viewTodo todo =
    if not todo.editing then
        li
            [ style "width" "100%"
            , style "display" "flex"
            , style "justify-content" "space-between"
            ]
            [ button [ onClick (ToggleTodoStatus todo.id)
                , style "font-size" "2.5rem"
                , style "color" "grey"
                , style "border" "none"
                , style "margin-top" "2rem"
                , style "background" "none"
                ]
                    [ text
                        (if todo.completed then
                            "✓"

                         else
                            "〇"
                        )
                    ]
            , span
                [ style "text-decoration"
                    (if todo.completed then
                        "line-through"

                     else
                        "none"
                    )
                , id (String.fromInt <| todo.id)
                , style "font-size" "4rem"
                , style "margin-top" ".5rem"
                ]
                [ text todo.text ]
            , button [ onClick (EditTodo todo.id)
                    , style "font-size" "2.5rem"
                    , style "color" "grey"
                    , style "border" "none"
                    , style "margin-top" "2rem"
                    , style "background" "none"
                    ] [ text "Edit" ]
            , button [ style "font-size" "2.5rem"
                    , style "color" "grey"
                    , style "border" "none"
                    , style "background" "none"
                    , style "margin-top" "2rem"
                    , onClick (RemoveTodo todo.id)
                    ] 
                    [  text "❌" ] 
            ]

    else
        li
            [ style "width" "100%"
            , style "display" "flex"
            , style "justify-content" "space-between"
            ]
            [ input
                [ type_ "text"
                , value todo.text
                , style "font-size" "2.5rem"
                , style "color" "grey"
                , style "background" "none"
                , style "margin-top" "2rem"
                , onInput (EditTodoInput todo.id)
                ]
                []
            , button 
                [ style "font-size" "2.5rem"
                , style "color" "grey"
                , style "border" "none"
                , style "background" "none"
                , style "margin-top" "2rem"
                , onClick (UpdateTodo todo.id)
                ] 
                [ text "Save" ]
            , button [ style "font-size" "2.5rem"
                    , style "color" "grey"
                    , style "border" "none"
                    , style "background" "none"
                    , style "margin-top" "2rem"
                    , onClick (RemoveTodo todo.id)
                    ] 
                    [  text "❌" ] 
            ]


loginFormView : Model -> Html Msg
loginFormView model =
    div 
        [ class "container" ]
        [ br [] []
        , viewInfo "Login"
        , inputUsername model
        , br [] []
        , inputPassword model
        , br [] []
        , loginButton
        , br [] []
        , p 
            [ style "color" "grey", style "text-align" "center", style "font-size" "1.5rem" ] 
            [ text "Or click on Signup to Create a New Account" ]
        , formToggleButton "Signup"
        ]

signupFormView : Model -> Html Msg
signupFormView model =
    div 
        [ class "container" ]
        [ br [] []
        , viewInfo "Signup"
        , inputUsername model
        , br [] []
        , inputPassword model
        , br [] []
        , signupButton
        , p 
            [ style "color" "grey", style "text-align" "center", style "font-size" "1.5rem" ] 
            [ text "Or click on Login to Login to your Account" ]
        , formToggleButton "Login"
        ]

viewInfo : String -> Html msg
viewInfo formTitle =
    div 
        [] 
        [ h1 
            [ style "color" "red", style "text-align" "center", style "font-size" "10rem" ] 
            [ text "todos" ]
        , p 
            [ style "color" "grey", style "text-align" "center", style "font-size" "3rem" ] 
            [ text "A simple todo-app build using Elixir & Elm." ]
        , br [] []
        , h3 
            [ style "color" "grey", style "text-align" "center", style "font-size" "6rem" ] 
            [ text formTitle ]
        ]

inputUsername : Model -> Html Msg
inputUsername model =
    div 
        [ style "width" "50%", style "margin" "auto" ]
        [ span 
            [ style "color" "grey", style "text-align" "center", style "font-size" "2rem" ] 
            [ text "Enter Username" ]
        , br [] []
        , input
            [ type_ "text"
            , placeholder "Username"
            , Html.Attributes.value model.username
            , onInput ChangeUsername
            , style "color" "grey"
            , style "font-size" "2rem"
            ]
            []
        ]

inputPassword : Model -> Html Msg
inputPassword model =
        div 
        [ style "width" "50%", style "margin" "auto" ]
        [ span 
            [ style "color" "grey"
            , style "text-align" "center"
            , style "font-size" "2rem"
            ] 
            [ text "Enter Password" ]
        , br [] []
        , input
            [ type_ "password"
            , placeholder "Password"
            , Html.Attributes.value model.password
            , onInput ChangePassword
            , style "color" "grey"
            , style "font-size" "2rem"
            ]
            []
        ]

loginButton : Html Msg
loginButton =
    div 
        [ style "width" "50%", style "margin" "auto" ]
        [ button 
            [ type_ "button"
            , class "btn"
            , onClick Msgs.Login
            , style "background" "red"
            , style "width" "100%"
            , style "font-size" "2rem"
            ]
            [ text "Login" ]
        ]

signupButton : Html Msg
signupButton =
    div 
        [ style "width" "50%", style "margin" "auto" ]
        [ button 
            [ type_ "button"
            , class "btn"
            , onClick SignUp
            , style "background" "red"
            , style "width" "100%"
            , style "font-size" "2rem"
            ]
            [ text "Signup" ]
        ]

formToggleButton : String -> Html Msg
formToggleButton buttonText =
    div 
        [ style "width" "50%", style "margin" "auto" ]
        [ button 
            [ type_ "button"
            , class "btn"
            , onClick ToggleSignUp
            , style "background" "red"
            , style "width" "100%"
            , style "font-size" "2rem"
            ]
            [ text buttonText ]
        ]

logoutButton : Html Msg
logoutButton =
    button 
        [ type_ "button"
        , class "btn"
        , onClick Logout
        , style "background" "red"
        , style "width" "25%"
        , style "font-size" "2rem"
        , style "float" "right"
        , style "margin" "auto"
        ]
        [ text "Logout" ]

clearButton : Html Msg
clearButton =
    button 
        [ type_ "button"
        , class "btn"
        , onClick Clear
        , style "background" "red"
        , style "font-size" "1.5rem"
        , style "float" "right"
        , style "margin" "auto"
        , style "font-size" "3.5rem"
        , style "width" "100%"
        , style "height" "10%"
        , style "padding" "10px"
        , style "border-radius" "4px"
        , style "border" "1px solid #333"
        ]
        [ text "Clear All" ]