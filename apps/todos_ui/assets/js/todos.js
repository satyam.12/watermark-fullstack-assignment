import "phoenix_html"

const Elm = require('../elm/Todos/Main.elm')

var div = document.getElementById("elm-main")

if (div) {
	var app = Elm.Elm.Main.init({node: div});
} else {
	console.log("Elm element not found!")
}