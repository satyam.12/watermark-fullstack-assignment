defmodule TodosUiWeb.TodosController do
  use TodosUiWeb, :controller

  def login(conn, _params) do
    render(conn, "login.html")
  end
end
